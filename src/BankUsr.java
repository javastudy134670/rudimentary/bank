
class BankUsr {
	static int totality = 0; // sum total of users
	String name;
	long idCard; // idCard number
	double balance;
	int uid; // usr id in bank

	BankUsr() {// empty
		cancelAcount();
	}

	BankUsr(String name, long idCard) {
		newAcount(name, idCard);
	}

	private void newAcount(String name, long idCard) {
		this.name = name;
		this.idCard = idCard;
		uid = ++totality;
		balance = 0;
	}

	public boolean isActive() {
		return (uid > 0);
	}

	public void cancelAcount() {
		name = "";
		balance = uid = -1;
	}

	public double getBalance() {
		if (!isActive()) {
			System.err.println("error! this acount is not active");
			return -1;
		}
		return balance;
	}

	public int withdraw(double amount) {
		if (!isActive()) {
			System.err.println("error! this acount is not active");
			return -1;
		}
		if (amount > balance) {
			System.err.println("error! balance is insufficient to withdraw $" + amount + ".");
			System.err.println("you have $" + balance);
			return 0x1;
		}
		if (amount > 2e5) {
			System.err.println("error! you cannot withdraw money more than $20,000 at one time.");
			return 0x2;
		}
		balance -= amount;
		return 0;
	}

	public void deposit(double amount) {
		if (!isActive()) {
			System.err.println("error! this acount is not active");
			return;
		}
		balance += amount;
	}

	public void getBasicInfo() {
		if (!isActive()) {
			System.err.println("error! this acount is not active");
			return;
		}
		System.out.println("name: " + name);
		System.out.println("idCard: " + idCard);
		System.out.println("uid: " + uid);
		System.out.println("balance: " + balance);
	}
}
