import java.util.Scanner;

public class Bank {
	final int maxNumOfUsrs = 5000;
	int usrNum = 0;
	BankUsr[] usrs = new BankUsr[maxNumOfUsrs];
	static Scanner input = new Scanner(System.in);

	public static void main(String[] argv) {
		Bank bank = new Bank();
		while (true) {
			int uid=-2;
			while (uid == -2) {
				uid = bank.login();
				switch (uid) {
					case 0 -> { // user choose to quit.
						thanks();
						return;
					}
					case -1 -> { // create a new user acount
						System.out.println("#create...");
						System.out.println("please input your name and idCard number:");
						String name = input.next();
						long idCard = input.nextLong();
						uid = ++bank.usrNum;
						bank.usrs[uid] = new BankUsr(name, idCard);
					}
				}
			}
			int key = 1;
			while (key != 0) {
				key = bank.menu();
				switch (key) {
					case 0 -> System.out.println("#logout...");
					case 1 -> {
						System.out.println("#withdraw...");
						System.out.println("how much money do you want to withdraw?");
						bank.usrs[uid].withdraw(input.nextDouble());
						System.out.println("secucess: the balance of your acount remains $" + bank.usrs[uid].balance);
					}
					case 2 -> {
						System.out.println("#deposit...");
						System.out.println("how much money do you want to deposit?");
						bank.usrs[uid].deposit(input.nextDouble());
						System.out.println("secucess: the balance of your acount remains $" + bank.usrs[uid].balance);
					}
					case 3 -> {
						System.out.println("#listInformation...");
						bank.usrs[uid].getBasicInfo();
					}
					case 4 -> {
						System.err.println("#CANCEL ACOUNT...");
						System.out.println("are you sure you want to cancel this acount?(y/N)");
						String sureToCancel = input.next();
						if (sureToCancel.equalsIgnoreCase("y")) {
							bank.usrs[uid].cancelAcount();
							System.out.println("this acount has been cancel");
							key = 0;
						}
					}
					default -> System.err.println("#optionNotExist...");
				}
			}
		}
	}

	private int login() {
		System.out.println("#login...");
		System.out.println(
				"please input your name and your bankUser id (press 0 to quit, press -1 to create a new acount):");
		int uid = input.nextInt();
		if (uid == 0 || uid == -1) {
			return uid;
		} else if (uid > usrNum || uid < -1 || !usrs[uid].isActive()) {
			System.err.println("error! this acount is not active");
			System.out.println("please login again.");
			return -2;
		} else
			return uid;
	}

	private int menu() {
		System.out.println("#menu...");
		System.out.println("==========================================");
		System.out.println("Select from the following options:");
		System.out.println("0 for quit");
		System.out.println("1 for withdraw;");
		System.out.println("2 for deposit;");
		System.out.println("3 for show the basic information of my acount;");
		System.out.println("4 for cancel this acount");
		return input.nextInt();
	}

	private static void thanks() {
		System.out.println("thank you for using!");
		System.out.println("==========================================");
	}
}
